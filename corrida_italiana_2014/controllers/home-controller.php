<?php 

/**
*  Controller for Home Page
* 
*
*/
        
render('head', [

    'title' => 'Bienvenido a la página oficial de la Corrida Italiana 2014'

]);
render('navbar');

load_module('carrusel-inicio');
load_module('inicio');
load('sponsor-column');

load_module('recorridos');
       
render('footer');