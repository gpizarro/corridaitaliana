<?php 

/**
*  Controller:
*       Enviar Recordatorio de pago
*
* */


if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
             
                    
                 // send the email 
                require( ROOT.'/Corrida/reminders/send_reminder_mail.php');
                
                 load_admin_base_template('success', [
            
                'page_name'     => '¡Listo!',
                'message'       => 'Los recordatorios se enviaron a los que aún no han pagado.',
                'return_path'   => 'ver-inscritos'
            
            ]);
                
            }  else {
             
           load_admin_base_template('enviar-recordatorio', [
            
                'page_name'           => 'Enviar correo masivo',
                'descripcion_mensaje' => 'Este correo se enviará a los inscritos que aún no han pagado'
            
            ]);  
                          
         }
