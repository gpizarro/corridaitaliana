<?php 


if( isset( $_GET['location'] ) ) {
    
    $location = $_GET['location'];

} else {
    
    $location = 'home';

}


switch ($location) {
    
    case 'home':
        
        load_controller('home');
        
    break;
    
    
    case 'inscripciones':
    
        load_base_template_with('inscripciones', [
        
            'title' => 'Inscripciones'
        
        ]);
    
    break;
    
    
    case 'acerca-de-la-corrida':
    
    
        load_base_template_with('acerca-de-la-corrida', [
        
            'title' => '¿Por qué la Corrida Italiana?'
        
        ]);
        
    break;
    
        
    case 'reglamento':
    
        load_base_template_with('reglamento', [
        
            'title' => 'Reglamento'
        
        ]);
    
    break;
    
    
    case 'contacto':
        
        load_base_template_with('contacto', [
        
            'title' => 'Contacto'
        
        ]);
        
    break;
    

    case 'contactar':
        
        load_controller('contact');
        
    break;
    
    
    case 'registrar':
        
        load_controller('register');
        
    break;
    
    
    case 'admin':
    
        load_controller('admin');            
    
    break;
    
    
    case 'recorridos':
        
        render('head');
        render('navbar');
        
        load('recorridos');
        
        render('footer');
        
    break;

}
    