<?php namespace Corrida\Corredores;

use Corredores;

// Validate the form
require ROOT.'/Corrida/registration/registration_validator.php';

if( $validated == true ) {

    //  Insert data into database
    
    $corredor = Corredores::create([
    
        'nombre'            => $nombre,
        'apellido_paterno'  => $apellido_paterno,
        'apellido_materno'  => $apellido_materno,
        'sexo'              => $sexo,
        'fecha_nacimiento'  => $fecha_nacimiento,
        'rut'               => $rut,
        'telefono'          => $telefono,
        'email'             => $email,
        'distancia'         => $distancia,
        'talla'             => $talla,
        'tipo_corredor'     => $tipo_corredor

        
    ]);
    
    
     
    // Load to registered page
    
    $monto = '';
    
    switch ($tipo_corredor) {
        
        case 'alumno':
            $monto = '$2.000';
        break;
        
        case 'ex_alumno':
        case 'apoderado': 
            $monto = '$3.500';
        break;
        
        case 'funcionario':
            $monto = '$2.500';
        break;
        
        case 'externo':
            $monto = '$7.000';
        break;
        
        
    };
    
    // Send the confirmation email
    require ROOT.'/Corrida/registration/send_registered_mail.php';

        
    load_base_template_with('registrado', [
    
        'nombre'            => $nombre,
        'apellido_paterno'  => $apellido_paterno,
        'apellido_materno'  => $apellido_materno,
        'sexo'              => $sexo,
        'fecha_nacimiento'  => $fecha_nacimiento,
        'rut'               => $rut,
        'telefono'          => $telefono,
        'email'             => $email,
        'distancia'         => $distancia,
        'talla'             => $talla,
        'tipo_corredor'     => $tipo_corredor,
        'monto'             => $monto
        
    ]);
    
    

} else {
            
    load_base_template_with('inscripciones', [
    
        'errors' => $errors
    
    ]);
    
}
