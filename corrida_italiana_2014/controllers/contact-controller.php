<?php namespace Corrida\Contactos;

use Contactos;

/**
* Contact Controller
* 
*
*/


// Validate this form

require ROOT.'/Corrida/contact/contact_validator.php';


// Load page and post info to db

if ( $validated == true ) {
                
        
    // Insert data in the database

    $contact = Contactos::create([
    
        'nombre'    => $nombre,
        'email'     => $email,
        'telefono'  => $telefono,
        'mensaje'   => $mensaje
        
    
    ]);
    
    // Load 'message sent' page
        
    load_base_template_with('contacto-recibido', [
    
        'title'     => 'Tu mensaje ha sido enviado',
        'nombre'    => $nombre,
        'email'     => $email,
        'telefono'  => $telefono,
        'mensaje'   => $mensaje
    
    ]);

    
    
}  else {
    
    load_base_template_with('contacto', [

        'title' => 'Error en el formulario',
        'errors' => $errors
    
    ]);
    
}


