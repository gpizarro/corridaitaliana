<?php namespace Corrida;

use Corredores;
use Contactos;

if (isset($_GET['action'] )) {
    
    $action = $_GET['action'];
    
} else {

    $action = 'general';
}



switch ($action) {
    
    case 'general':
    
        $unpaid     = count(Corredores::find_by_sql('SELECT * FROM corredores where pagado is NULL'));
        $paid       = count(Corredores::find_by_sql('SELECT * FROM corredores where pagado = 1'));
        
        load_admin_base_template('general', [
        
            'total_corredores'  => Corredores::count(),
            'unpaid'            => $unpaid,
            'paid'              => $paid
        ]);

    break;
    
    
    case 'ver-inscritos':
    
        $corredores = new Corredores;
        $corredores = Corredores::all();
        $unpaid     = count(Corredores::find_by_sql('SELECT * FROM corredores where pagado is NULL'));
            
        if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
            
            $pagaron = $_POST['pagado'];
                        
            Corredores::update_all(array(
              'set' => array(
                'pagado' => 1
              ),
              'conditions' => array(
                'id' => $pagaron
              )
            ));
            
            
            load_admin_base_template('success', [
            
                'page_name'     => '¡Listo!',
                'message'       => 'Los pagos se actualizaron exitósamente.',
                'return_path'   => 'ver-inscritos'
            
            ]);
        
        } else {
        
        load_admin_base_template('inscritos', [
        
            'page_name'     => 'Inscritos a la corrida',
            'corredores'    => $corredores,
            'unpaid_count'  => $unpaid
            
            ]);
            
        }
    
    break;
    
    
    case 'enviar-recordatorio':
    
         load_controller('enviar-recordatorio');    
        
    
    break;
    
    case 'ver-perfil':
    
        $id       = $_GET['id'];
        $corredor = Corredores::find($id);
        
        if( isset( $_POST['pagado']) ) {
            
            $corredor->pagado = 1;
            $corredor->save();
            
            load_admin_base_template('perfil', [
            
                'corredor' => $corredor,
                'alert'     => true
        
            ]); 
            
        } else {
            
              load_admin_base_template('perfil', [
            
                'corredor' => $corredor
        
            ]);  
            
        }
        
        
        
    break;
    
    case 'contactos':
    

        load_admin_base_template('contactos', [
        
            'page_name' => 'Contactos',
            'contactos' => Contactos::all([
            
                'order' => 'id desc'
            
            ])
        
        ]);

    break;
    
}

        
