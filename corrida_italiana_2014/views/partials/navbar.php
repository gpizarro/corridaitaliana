<!-- Navegación -->
<div class="navbar navbar-default navbar-fixed-top"
     role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button"
                 class="navbar-toggle collapsed"
                 data-toggle="collapse"
                 data-target=".navbar-collapse"><span class="sr-only">Toggle Nav</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button> <a class="navbar-brand"
                 href="<?= HOST ?>home">Corrida Italiana 2014</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <?php link_to('reglamento', 'Reglamento') ;?>
                </li>
                <li>
                    <?php link_to('acerca-de-la-corrida', 'Acerca de la corrida') ;?>
                </li>
                
                <li>
                    <?php link_to('recorridos', 'Recorridos') ;?>
                </li>
                
                <li>
                    <a href="https://www.facebook.com/corrida.italiana" target="_blank">Facebook</a>
                </li>
               <!--
 <li>
                    <?php link_to('auspiciadores', 'Auspiciadores') ;?>
                </li>
-->
                <!--
<li>
                    <?php link_to('carreras-pasadas', 'Carreras pasadas') ;?>
                </li>
-->
               <!--
 <li>
                    <?php link_to('contacto', 'Contacto') ;?>
                </li>
-->
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php link_to('inscripciones', 'Inscripciones') ;?>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
