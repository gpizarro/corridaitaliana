<footer class='footer'>
    
    <div class="container">
    
        <div class="row">
            <div class="col-md-1">
                <img src="<?= HOST ;?>images/logo-corrida-footer.png" alt="" class="img-responsive logo-corrida">    
            </div>
       
            <div class="col-md-11">
               <p class="title">CORRIDA ITALIANA 2014</p>
            </div>
        </div>
       
       <div class="row">
           <div class="col-md-12 copyright">
             <p>&copy; <?= date('Y') ;?> - Corrida Italiana - Todos los derechos reservados</p>
            </div>
       </div>
              
    </div>
    
</footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>