        <div class="alert alert-danger" role="alert">
            
            <p>Hay unos errores:</p>
            
            <ul>
                <?php foreach($errors as $error) : ?>
                    <li><?= $error ;?></li>
               <?php endforeach ;?> 
            </ul>
            
        </div>

