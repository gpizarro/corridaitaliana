<h1>¡Listo! <br>
    <small>¡Ya estás <?= ($sexo == 'f') ? 'registrada' : 'registrado' ;?> para la Corrida Italiana! </small>
</h1>
    
    <p>Ahora debes validar tu registro. Para esto debes:
        
        <ol>
            
            <li>Transferir el monto de <strong><?= $monto ;?></strong> de la corrida a la siguiente cuenta bancaria:      
            
            
                <ul>
                	<li> Cuenta Vista N° 0-011-00-15948-8, Banco Santander</li>
                	<li> Nombre: Flavio Brusoni Costoya</li>
                	<li> RUN 18.642.261-9</li>
                	<li> Email: <?php mail_to('pagos@corridaitaliana.cl', 'pagos@corridaitaliana.cl') ;?></li>
                </ul>
            
            </li>
            
            <li>Envía el comprobante de pago a esta dirección, ya sea en caso de depósito por caja o transferencia electrónica.</li>
            
        </ol>
        
    </p>
    
    <p>Dentro de las próximas 72 horas recibirás un correo electrónico confirmando la validación de tu inscripción, donde será señalada la información sobre el retiro de tu KIT de competencia. El pago de la inscripción es individual.</p>
    
    <p>Si deseas pagar la inscripción de más una persona al mismo tiempo, debes enviar un email a <?php mail_to('pagos@corridaitaliana.cl', 'pagos@corridaitaliana.cl') ;?> (aparte del comprobante de pago) especificando el nombre y apellido de las personas a las que deseas incluir en tu pago.</p>
    
    <p class="lead">Gracias por inscribirte en la Corrida Italiana 2014. </p>
    
    <p class='lead'> Te esperamos el próximo 16 de noviembre.</p>
    
    <br>         
    <h3>Estás <?= ($sexo == 'f') ? 'registrada' : 'registrado' ;?> con los siguientes datos:</h3>       
    
    <div class="well">
        <h4>Nombre:</h4>
        <p><?= $nombre.' '.$apellido_paterno.' '.$apellido_materno ;?></p>
        
        <h4>Sexo:</h4>
        <p><?= ($sexo == 'f') ? 'Femenino' : 'Masculino' ;?></p>
        
        <h4>Fecha nacimiento:</h4>
        <p><?= $fecha_nacimiento ; // fix this to proper date format?></p>
        
        <h4>RUT:</h4>
        <p><?= $rut ;?></p>
        
        <h4>Teléfono:</h4>
        <p><?= $telefono ;?></p>
        
        <h4>Correo electrónico:</h4>
        <p><?= $email ;?></p>
    </div>
    
    <div class="well">
        <h4>Distancia:</h4>
        <p><?= $distancia ;?></p>
        
        <h4>Talla:</h4>
        <p><?= strtoupper( $talla );?></p>
        
        <h4>Tipo de corredor:</h4>
        <p><?= ucfirst( $tipo_corredor );?></p>
    </div>
    
    <p>Si algunos de estos datos están equivocados, envianos un email a <?php mail_to('mail@corridaitaliana.cl', 'mail@corridaitaliana.cl') ;?> para actualizar estos datos.</p>