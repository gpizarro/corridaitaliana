<div class="container">

    <h1>Recorridos de la Corrida 2014</h1>
	
	<!-- 2K -->
	<div class="anchor" id="2k"></div>
	<div class="row">
    	
    	<div class="col-md-12">
    	
    	    <div class="row">
    	    
    	        <div class="col-md-8">
        		
    	    		<img src="<?= HOST.'images/recorridos/2K.jpg' ;?>" alt="2K" class="img-responsive text-center" />

                    <br>
    		
                </div>
    	        
    	        <div class="col-md-4">
        	        
        	        	<h3 class="dosk">2K</h3>
        	        	<br>
        	        	
        	        	<h4 class="text-center">CARRERA DE DOS KILOMETROS</h4>
        	        	<br>
        	        	<ol>
                            <li>Parte en <strong>Avenida La Plaza</strong> hacia el Norte.</li>
                            <li>Vuelve hacia el Sur por <strong>Avenida La Plaza</strong> donde termina la doble calzada.</li>
                            <li>Dobla a la izquierda hacia la <strong>Scuola Italiana</strong>.</li>
                            <li>Termina la carrera en la <strong>pista atlética</strong>.</li>
                        </ol>
        	        	

        	        	
        	        	<hr><br>
        	        	
        	        	<div class="otros-recorridos text-center">
        	        		<h3>Otros recorridos:</h3>
        	        		<h4 class="big-ass-number"><a href="#4k">4k</a></h4>
        	        		<h4 class="big-ass-number"><a href="#7k">7k</a></h4>
        	        	</div>

        	        
    	        </div>
    		
    		    
    		    
            </div>
    	
        </div>
    	
    	
	</div>
	
	<hr>
	
	<!-- 4k -->
	<div class="anchor" id="4k"></div>
	<div class="row">
    	
    	<div class="col-md-12">
    	
    	    <div class="row">
    	    
    	        <div class="col-md-8">
        		
    	    		<img src="<?= HOST.'images/recorridos/4K.jpg' ;?>" alt="4K" class="img-responsive text-center" />

                    <br>
    		
                </div>
    	        
    	        <div class="col-md-4">
        	        
        	        	<h3 class="cuatrok">4K</h3>
        	        	
        	        	<br>
        	        	
        	        	<h4 class="text-center">CARRERA DE CUATRO KILOMETROS</h4>
        	        	<br>
        	        	<ol>
                            <li>Parte en <strong>Avenida La Plaza</strong> hacia el Norte.</li>
                            <li><Dobla a la izquierda en <strong>Avenida San Francisco de Asís</strong>.</li>
                            <li>Dobla a la izquierda en <strong>Avenida San Carlos de Apoquindo</strong>.</li>
                            <li>Dobla a la izquierda en <strong>General Blanche</strong>.</li>
                            <li>Dobla a la izquierda en <strong>Avenida La Plaza</strong>.</li>
                            <li>Dobla a la derecha hacia la <strong>Scuola Italiana</strong>.</li>
                            <li>Termina la carrera en la <strong>pista atlética</strong>.</li>
                        </ol>
        	        	

        	        	
        	        	<hr><br>
        	        	
        	        	<div class="otros-recorridos text-center">
        	        		<h3>Otros recorridos:</h3>
        	        		<h4 class="big-ass-number"><a href="#2k">2k</a></h4>
        	        		<h4 class="big-ass-number"><a href="#7k">7k</a></h4>
        	        	</div>

        	        
    	        </div>
    		    
            </div>
    	
        </div>
    	
    	
	</div>
	
	<hr>
	
	<!-- 7k -->
	<div class="anchor" id="7k"></div>
	<div class="row">
    	
    	<div class="col-md-12">
    	
    	    <div class="row">
    	    
    	        <div class="col-md-8">
        		
    	    		<img src="<?= HOST.'images/recorridos/7K.jpg' ;?>" alt="7K" class="img-responsive text-center" />

                    <br>
    		
                </div>
                
                <div class="col-md-4">
        	        
        	        	<h3 class="sietek">7K</h3>
        	        	
        	        	<br>
        	        	
        	        	<h4 class="text-center">CARRERA DE SIETE KILOMETROS</h4>
        	        	<br>

                        <ol>
                            <li>Parte en <strong>Avenida La Plaza</strong> hacia el Norte.</li>
                            <li>Dobla a la izquierda en <strong>Avenida San Francisco de Asís</strong>.</li>
                            <li>Dobla a la izquierda en <strong>Avenida San Carlos de Apoquindo</strong>.</li>
                            <li>Dobla a la derecha en <strong>Cerro Catedral Sur</strong>.</li>
                            <li>Dobla a la izquierda en <strong>Cerro San Ramón</strong> (posteriormente <strong>San Ramón</strong>).</li>
                            <li>Dobla a la izquierda en <strong>Camino del Santuario</strong>.</li>
                            <li>Vuelve hacia el Norte por <strong>Avenida San Carlos de Apoquindo</strong>.</li>
                            <li>Entra por la derecha a la <strong>Universidad de Los Andes</strong>.</li>
                            <li>Continúa desde la universidad hacia <strong>Avenida La Plaza</strong>.</li>
                            <li>Dobla a la derecha hacia la <strong>Scuola Italiana</strong>.</li>
                            <li>Termina la carrera en la <strong>pista atlética</strong>.</li>
                        </ol>        	        	

        	        	
        	        	<hr><br>
        	        	
        	        	<div class="otros-recorridos text-center">
        	        		<h3>Otros recorridos:</h3>
        	        		<h4 class="big-ass-number"><a href="#2k">2k</a></h4>
        	        		<h4 class="big-ass-number"><a href="#4k">4k</a></h4>
        	        	</div>

        	        
    	        </div>    		    
            </div>
    	
        </div>
    	
    	
	</div>
	
</div>