<h1>Tu mensaje ha sido enviado</h1>

<p class="lead">Te contestaremos lo más pronto posible.</p>

<div class="well">
    
    <p>Este es el contenido de tu mensaje:</p>    
    
    
    <h3 class="small">Nombre:</h3>
    
    <?= $nombre ;?>
    
    
    <h3 class='small'>Correo Electrónico:</h3>
    
    <p><?= $email ;?></p>
    
    
    <h3 class='small'>Teléfono:</h3>
    
    <?= $telefono ;?>
    
    
    <h3 class='small'>Mensaje:</h3>
    <?= $mensaje ;?>
    
</div>


<?php link_to('home', 'Regresa a la página principal') ;?>