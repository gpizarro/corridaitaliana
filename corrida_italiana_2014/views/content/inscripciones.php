<h1>Inscripciones</h1>

    
     <?php if(!empty($errors) ) { render('form-errors', ['errors' => $errors ]); }?>
     

    <form class="navbar-form" role="search" method="post" action="?location=registrar">

        <div class="form-group">
        
            
            
            
            <div class="well clearfix">
                <h2>Datos Básicos</h2>

                <div class="row">
                	<div class="col-md-6">
                	    <label for="nombre">Nombre:</label>
                		<input type="text" class="form-control" placeholder="" id="nombre" name="nombre" value="<?= old('nombre') ;?>">
                		<br>
                	</div>
                </div>
                
                <div class="row">
                	
                	<div class="col-md-8">
                	    <label for="apellido_paterno">Apellido paterno:</label>
                	    <input type="text" class="form-control" placeholder="" id="apellido_paterno" name="apellido_paterno" value="<?= old('apellido_paterno') ;?>">
                	    <br>
                	</div>
                	
                	<div class="col-md-8">
                	    <label for="apellido_materno">Apellido materno:</label>
                	    <input type="text" class="form-control" placeholder="" id="apellido_materno" name="apellido_materno" value="<?= old('apellido_materno') ;?>">
                	    <br>
                	</div>
                	
                </div>
                
                <br>
                
                <div class="row">
                	<div class="col-md-8 btn-group-radio btn-group">
                	    
                	    
                	    <label for="sexo">Sexo:</label><br>
                	    <input type="radio" name="sexo" value="m" <?php old_radio('sexo', 'm') ;?>> Masculino
                        <br>
                	    <input type="radio" name="sexo" value="f" <?php old_radio('sexo', 'f') ;?>> Femenino
                	</div>
                	
                </div>
                
                <br>
                
                <div class="row">
                	<div class="col-md-8">
                	    <label for="fecha_nacimiento">Fecha de nacimiento:</label>
                	    <br>
                	    <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" value="<?= old('fecha_nacimiento') ;?>">
                	    
                	</div>
                
                	<br>
                	<div class="col-md-8">
                	    <label for="rut">RUT:</label>
                	    <input type="text" class="form-control" placeholder="" id="rut" name="rut" value="<?= old('rut') ;?>">
                	    <br>
                	</div>
                </div>
                
                <br>
                
                
                
                <div class="row">
                    <br>
                	<div class="col-md-8">
                	    <label for="telefono">Telefono:</label>
                	    <input type="text" class="form-control" placeholder="" id="telefono" name="telefono" value="<?= old('telefono') ;?>">
                	</div>
                </div>
                
                <div class="row">
                    <br>
                	<div class="col-md-8">
                	    <label for="email">Correo Electrónico:</label>
                	    <input type="email" class="form-control" placeholder="" id="email" name="email" value="<?= old('email') ;?>" >
                	</div>
                </div>
            
            </div>

            
            <div class="well clearfix">
                <h2>Datos de la corrida</h2>
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="distancia">Distancia:</label><br>
                        <input type="radio" name="distancia" value="2k" <?php old_radio('distancia', '2k') ;?>> 2K<br>
                        <input type="radio" name="distancia" value="4k" <?php old_radio('distancia', '4k') ;?>> 4k<br>
                        <input type="radio" name="distancia" value="7k" <?php old_radio('distancia', '7k') ;?>> 7k<br>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-8">
                        <label for="talla">Talla Polera:</label>
                        <br>
                        <input type="radio" name="talla" value="xs" <?php old_radio('talla','xs') ;?>> XS <br>
                        <input type="radio" name="talla" value="s"  <?php old_radio('talla','s') ;?>> S    <br>
                        <input type="radio" name="talla" value="m"  <?php old_radio('talla','m') ;?>> M    <br>
                        <input type="radio" name="talla" value="l"  <?php old_radio('talla','l') ;?>> L    <br>
                        <input type="radio" name="talla" value="xl" <?php old_radio('talla','xl') ;?>> XL  <br>
                        <input type="radio" name="talla" value="xxl"<?php old_radio('talla','xxl') ;?> > XXL<br>
                        <br>
                    </div>
                </div>
                
                <h3>Acerca del corredor</h3>
                
                <div class="row">
                    <div class="col-md-8">
                	
                		
                		<input type="radio" id="alumno" name="tipo_corredor" value="alumno"  <?php old_radio('tipo_corredor','alumno') ;?>
                		<label for="alumno">&nbsp; Alumno Scuola ($2.000) </label>
                		<br>
                		
                		
                		<input type="radio" id="exalumno" name="tipo_corredor" value="ex_alumno"  <?php old_radio('tipo_corredor','ex_alumno') ;?>
                		<label for="exalumno">&nbsp; Ex-Alumno ($3.500) </label>
                		<br>
                		
                		
                		<input type="radio" id="funcionario" name="tipo_corredor" value="funcionario"  <?php old_radio('tipo_corredor','funcionario') ;?>
                		<label for="funcionario">&nbsp; Funcionario ($2.500)</label>
                		<br>
                		
                		
                		<input type="radio" id="apoderado" name="tipo_corredor" value="apoderado"  <?php old_radio('tipo_corredor','apoderado') ;?>
                		<label for="apoderado">&nbsp; Apoderado ($3.500) </label>
                		<br>
                		
                		
                		<input type="radio" id="externo" name="tipo_corredor" value="externo"  <?php old_radio('tipo_corredor','externo') ;?>
                		<label for="externo">&nbsp; Externo ($7.000) </label>
                		<br>
                        </div>
                </div>
                
                
            </div>
        </div>
        
        <article class="articulo">
                                <h3>Pliego de Descargo de Responsabilidades y Protección de datos.</h3>
                            
                            <p>Por el sólo hecho de inscribirse, el participante declara lo siguiente:</p>
                            
                            <p><em>“Me encuentro en estado de salud óptima de participar en la IV Corrida Italiana. Además eximo de toda responsabilidad a la organización, auspiciadores, patrocinadores u otras instituciones participantes, ante cualquier accidente o lesión que pudiera sufrir antes, durante y/o después del evento deportivo, renunciando desde ya a cualquier acción legal en contra de dichas entidades públicas o privadas.</em></p>
                            
                            <p><em>Entiendo y me encuentro conforme con las condiciones establecidas precedentemente, junto con las responsabilidades que como participante tengo y declaro que no haré responsable a la organización por incumplimientos propios.</em></p>
                            
                            <p><em>Durante el desarrollo de la competencia, contribuiré en lo posible con la organización para evitar accidentes personales. Autorizo además, a que la organización haga uso publicitario de fotos, videos y cualquier otro tipo de material audiovisual en que pueda figurar, aceptando la publicación de mi nombre en la clasificación de la prueba, en los medios de comunicación social y/o Internet, sin esperar pago, compensación o retribución alguna por este concepto. Acepto que lo anterior es condición necesaria para retirar el número de competencia y participar en la Corrida Italiana año 2014.”</em></p>
                            </article>


        <div class="col-md-4 col-md-offset-3 register">
        	<button type="submit" class="corrida-btn big-btn" >Inscribete</button>
        </div>

    </form>