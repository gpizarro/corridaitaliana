<h1>Propósito</h1>

<p>La Corrida Italiana es de la familia de la Scuola para otras familias.</p>

<p>Nos gusta decir eso porque se ha transformado en una gran oportunidad de enriquecer nuestra cultura, tradición y la vocación de servicio que aprendimos desde muy niños. La definimos de esa manera porque tenemos la convicción de que no es un proyecto que sólo busque recaudar dinero. Es cierto, nace porque necesitábamos más fondos, pero ha crecido y hoy comprende mucho más que eso. La corrida se ha transformado en un espacio para que nuestra comunidad pueda compartir y conocerse haciendo deporte en familia.</p>

<img class="img-responsive" src="<?= HOST ;?>images/Corrida-Italiana-TV.jpg">	        

<p>El proyecto nace con la idea de financiar los trabajos sociales del Scout de la Scuola Italiana el que pretende generar un impacto social apadrinando un pueblo de escasos recursos por más de dos años. Aquí se realizan talleres profesionales a padres y jóvenes, obras de teatro y café concert de gran calidad y diversos tipos de juegos para los niños. Ha servido de base para el trabajo coordinado con los municipios, con propuestas desafiantes y llevadas a cabo. Finalmente, la corrida ha sido el lugar donde se reúne la familia y se ha transformado en el pilar fundamental para uno de los aportes a la sociedad más importantes de la Scuola.</p>

<p>Ven a la Corrida Italiana, disfruta, aporta y siente su vida propia. Es la oportunidad para compartir y comprometerte con aquellos que tiene menos pero que también tienen derecho a reír, jugar y aprender.</p>


<br>

<div class="embed-responsive embed-responsive-16by9">

<iframe width="560" height="315" src="//www.youtube.com/embed/f-E1E9LD418" frameborder="0" allowfullscreen></iframe>

</div>

<p>La corrida es del Grupo Scout San Francesco, es de la Scuola Italiana Vittorio Montiglio, es de nuestra familia.</p>