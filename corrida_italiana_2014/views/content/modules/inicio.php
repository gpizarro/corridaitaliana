<!-- Modulo Home -->
<section class="home-module">
	<div class="container"> 
	
	    <div class="row">
	       	        
            <div class="col-md-12">            
            
                <div class="col-md-8 row">
                
                <h2>La Corrida Italiana 2014</h2>
                
                <div class="row module-content">
                    
    	            <!-- Inscribete aquí -->
    	            <div class="col-md-6">
                        <img class="img-responsive" src="<?= HOST ;?>images/corrida2013-047.jpg">	        
                    </div>
                    
                    <div class="col-md-6">
        	    	    	            
        	            <p>¡Las inscripciones para la Corrida Italiana 2014 ya están abiertas!</p>
        	            <br>
                        <?php link_to('inscripciones', 'Inscríbete aquí', 'corrida-btn') ;?>
                        
                    </div>
                
                </div>  
                
                
                <h2>¿Por qué la corrida?</h2>             
                
                <div class="row module-content">
                                   
                    <div class="col-md-6">
                            
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe width="560" height="315" src="//www.youtube.com/embed/f-E1E9LD418" frameborder="0" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                    
                    <div class="col-md-6">    
                            <p>La Corrida Italiana es de la familia de la Scuola para otras familias.
Nos gusta decir eso porque se ha transformado en una gran oportunidad de enriquecer nuestra cultura, tradición y la vocación de servicio que aprendimos desde muy niños.</p>

                            <?php link_to('acerca-de-la-corrida', 'Conoce más', 'corrida-btn') ;?>
                        
                        </div>
                
                </div>   
	       </div> <!-- col-md-8 -->
	       
                       
        	       
<!-- Sponsors -->


        	<aside class="col-md-4 sponsor-col">
        	    <?php show_sponsor('scuola.jpg') ;?>
        	    <?php show_sponsor('romanini.jpg') ;?>
        	    <?php show_sponsor('uandes.jpg') ;?>
        	
        	</aside>
       
       </div> <!-- col-md-12 -->
        
        </div>
    	       
    	    
        
    </div> <!-- container -->
</section>
