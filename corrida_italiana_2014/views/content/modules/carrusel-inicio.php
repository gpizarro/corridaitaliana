<!-- Carrusel Inicio -->

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?= HOST ;?>images/slider-red-bg.jpg" alt="Corrida Italiana 2014" class="img-responsive">      
      
    </div>
    
    <div class="item">
      <img src="<?= HOST ;?>images/slider-green-bg.jpg" alt="Corrida Italiana 2014" class="img-responsive">

    </div>
    
    
    <div class="item">
      <img src="<?= HOST ;?>images/slider-runner-bg.jpg" alt="Corrida Italiana 2014" class="img-responsive">

    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>