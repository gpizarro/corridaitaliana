<h1 class="">BASES CORRIDA ITALIANA AÑO 2014</h1>

<nav class="well" role="navigation">
            	                 <h2>Índice</h2>
            	                 <ul class="list-unstyled">
            	                 	<li><a href="#articulo1">ARTÍCULO 1° Organización.</a></li>
            	                 	<li><a href="#articulo2">ARTÍCULO 2° Horarios.</a></li>
            	                 	<li><a href="#articulo3">ARTÍCULO 3° Inscripciones.</a></li>
            	                 	<li><a href="#articulo3b">ARTÍCULO 3 BIS. Valor de las inscripciones.</a></li>
            	                 	<li><a href="#articulo4">ARTÍCULO 4° Categorías.</a></li>
            	                 	<li><a href="#articulo5">ARTÍCULO 5° Entrega de kit de competencia.</a></li>
            	                 	<li><a href="#articulo6">ARTÍCULO 6° Premios.</a></li>
            	                 	<li><a href="#articulo7">ARTÍCULO 7° Obligaciones del participante.</a></li>
            	                 	<li><a href="#articulo8">ARTÍCULO 8° Servicios al participante.</a></li>
            	                 	<li><a href="#articulo9">ARTÍCULO 9° Reclamos.</a></li>
            	                 	<li><a href="#articulo10">ARTÍCULO 10° Vehículos en el recorrido.</a></li>
            	                 	<li><a href="#articulo11">ARTÍCULO 11° Responsabilidades</a></li>
            	                 	<li><a href="#articulo12">Pliego de Descargo de Responsabilidades y Protección de datos.</a></li>
            	                 </ul>
        	                </nav>
   	                
<div class="anchor" id="articulo1"></div>
<article class="articulo">
        	                	<h3>ARTÍCULO 1° Organización.</h3>
        	                	<p>La Scuola Italiana “Vittorio Montiglio” organiza e invita a: la cuarta versión de la Corrida Italiana año 2014, a celebrarse el día 16 de Noviembre en tres distancias: 2, 4 y 7 kilómetros. </p>
        	                	
	                            <p>El Director de la competencia será el ilustre Sr. Ennio Angelo Romanini Dupont.</p>
	
	                            
        	                </article>

<div class="anchor" id="articulo2"></div>
<article class="articulo">
        	                    <h3>ARTÍCULO 2° Horarios.</h3>
	                            <p>La competencia se llevará a cabo de acuerdo al siguiente programa:
	                                <ul>
	                                	<li>08:00 Llegada participantes</li>
	                                	<li>08:30 Calentamiento</li>
	                                	<li>08:45 posicionamiento corredores</li>
	                                	<li>09:00 Largada 2k, 4k y 7k</li>
	                                	<li>10:00 Elongación post-corrida</li>
	                                	<li>10:20 Inicio premiación</li>
	                                	<li>11:30 Término premiación y retirada de participantes</li>
	                                </ul>
	                                
	                            </p>
        	                </article>

<div class="anchor" id="articulo3"></div>
<article class="articulo">
            	                <h3>ARTÍCULO 3° Inscripciones.</h3>
                                <p>Las inscripciones se realizarán de dos formas, siendo facultativo para los participantes inscribirse de cualquiera de las dos siguientes formas:</p>
                            
                                    <article>
                                    	<h4>Inscripción On-line</h4>
                                    	
                                    	<p>Se realiza en el sitio web www.corridaitaliana.cl y posteriormente transfiriendo el valor de la corrida a la Cuenta Vista N° 0-011-00-15948-8 del Banco Santander, a nombre de Flavio Brusoni Costoya, RUN 18.642.261-9. El comprobante de depósito o transferencia debe ser enviado al correo electrónico <?php mail_to('pagos@corridaitaliana.cl', 'pagos@corridaitaliana.cl') ;?> </p>
                                    	
                                    </article>
                            
                                    <article>
                                    	<h4>Inscripción presencial</h4>
                                    	<p>Asistiendo a los diversos eventos que realizará la institución organizadora antes de la fecha de la Corrida, instancias en los cuales se abrirá un puesto de inscripción donde se podrá pagar en efectivo.</p>
                                    </article>
                            
                                <p>Las inscripciones se podrán realizar desde el Sábado 13 de Septiembre del 2014 hasta el Viernes 7 de Noviembre del mismo año (hora límite las 23:59hrs) o hasta agotar los 1500 cupos de competencia.</p>
                            
                                <p>Las inscripciones se harán efectivas llenando todos los campos o casilleros de la ficha de inscripción, la cual deberá ser leída y aceptada por el/la participante y posteriormente depositando el valor de la inscripción en la cuenta antes señalada.</p>
                            
                                <p>Al momento de inscribirse, cada participante se hace responsable por su seguridad durante la corrida, acepta este reglamento en todas sus partes, se somete a la competencia de los jueces de la corrida y declara que los datos que ha ingresado son fidedignos y se encuentran correctamente ingresados.</p>
                            
                                <p>La competencia tendrá un máximo de 1.500 vacantes de inscripción.</p>
                            
                                <p>La inscripción realizada por los medios anteriormente descritos da derecho al participante a recibir el kit de competencia, que incluye una polera técnica y el número de competidor</p>
                            
                                <p>Todo inscrito en el sistema puede participar del evento, pero solo podrán participar con derecho a premiación las personas que estén dentro del rango etario señalado en el <a href="#articulo5">artículo 5to</a>. </p>                        
                            
                                <p>No se permitirán cambios de distancia una vez ingresado el corredor al sistema.</p>
        	                </article>

<div class="anchor" id="articulo3b"></div>
<article class="articulo">
                                <h3>ARTÍCULO 3 BIS. Valor de las inscripciones.</h3>
                            
                                <p>La inscripción tendrá un valor de $7000 y contará con un descuento para las personas vinculadas a la Scuola Italiana Vittorio Montiglio. Para el participante que cuente con las siguientes calidades la inscripción tendrá los valores determinados a continuación:</p>
                            
                                <table class="table">
                                <tr>
                                    <td>Alumno de la Scuola Italiana</td>
                                    <td>$2.000</td>
                                </tr>
                                
                                <tr>
                                	<td>Funcionario de la Scuola Italiana</td>
                                	<td>$2.500</td>
                                </tr>
                                <tr>
                                	<td>Ex alumno y Apoderado de la Scuola Italiana</td>
                                	<td>$3.500</td>
                                </tr>
                                <tr>
                                	<td>Apoderado o Beneficiario del grupo scout San Francesco D'Assisi</td>
                                	<td>$3.500</td>
                                </tr>
                                
                                <tr>
                                	<td>Externo</td>
                                	<td>$7.000</td>
                                </tr>
                            </table>                   
                            
                                <p>El dinero recaudado se destinará a cubrir los costos de la actividad, y cuyas utilidades irán íntegramente dirigidas los trabajos de verano realizados por el Grupo scout San Francesco D’assisi, de la Scuola Italiana Vittorio Montiglio.</p>
                            </article>

<div class="anchor" id="articulo4"></div>
<article class="articulo">
                                 <h3>ARTÍCULO 4° Categorías.</h3>
                            
                                 <article>
                                     <h4>2 Kilómetros</h4>
                                     <p>Este recorrido no contará con categorías para los participantes</p>
                                 </article>
                                 
                            
                                 <article>
                                    <h4>4 Kilómetros </h4>
                                    <table class="table">
                                    	<thead>
                                        	<th>Categorías</th>
                                        	<th>Edad</th>
                                    	</thead>
                                    	
                                    	<tbody>
                                        	<tr>
                                            	<td>Infantil damas</td>
                                            	<td>7 a 14 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Infantil varones</td>
                                        		<td>7 a 14 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Juvenil damas</td>
                                        		<td>15 a 19 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Juvenil varones</td>
                                        		<td>15 a 19 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Adulto damas</td>
                                        		<td>20 a 39 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Adulto varones</td>
                                        		<td>20 a 39 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Sénior damas</td>
                                        		<td>Más de 40 años</td>
                                        	</tr>
                                        	
                                        	<tr>
                                        		<td>Sénior varones</td>
                                        		<td>Más de 40 años</td>
                                        	</tr>
                                    	</tbody>
                                    </table>
                                 </article>
                            
                            
                            <article>
                            <h4>7 Kilómetros</h4>
                            
                            <table class="table">
                            	<thead>
                            		<th>Categorías</th>
                            		<th>Edad</th>
                            	</thead>
                            	
                            	<tbody>
                                	<tr>
                                		<td>Juvenil damas</td>
                                		<td>15 a 19 años</td>
                                	</tr>
                                	<tr>
                                		<td>Juvenil varones</td>
                                		<td>15 a 19 años</td>
                                	</tr>
                                	<tr>
                                		<td>Adulto damas</td>
                                		<td>20 a 39 años</td>
                                	</tr>
                                	<tr>
                                		<td>Adulto varones</td>
                                		<td>20 a 39 años</td>
                                	</tr>
                                	<tr>
                                		<td>Sénior damas</td>
                                		<td>Más de 40 años</td>
                                	</tr>
                                	<tr>
                                		<td>Sénior varones</td>
                                		<td>Más de 40 años</td>
                                	</tr>
                            	</tbody>
                            </table>
                            </article>
                            <p><small>Edades correspondientes al día de la competencia. </small></p>
                            </article>

<div class="anchor" id="articulo5"></div>
<article class="articulo">
                                <h3>ARTÍCULO 5° Entrega de kit de competencia.</h3>
                            
                            <p>El kit de competencia se compondrá de la polera técnica oficial de la corrida y número del participante. Este será entregado personalmente a los participantes previamente inscritos, y es de su absoluta responsabilidad retirarlos en los horarios y días establecidos, so pena de perder su derecho a retiro. </p>
                            
                            <p>La entrega de estos se realizará el sábado 15 de Noviembre entre las 12:00 hrs. y las 19:00 hrs, en la Scuola Italiana “Vittorio Montiglio”, ubicada en Avda. Las flores 12707, San Carlos de Apoquindo, Las Condes. </p>
                            <p>En ese momento, el participante deberá firmar su declaración de consentimiento. Es de absoluta responsabilidad del participante retirar el kit, por ende, aquel que no lo haga pierde las prerrogativas contenidas en este reglamento -entre ellas la de participar en la competencia- y exime de toda responsabilidad a la organización.</p>
                            
                            <p>Cada participante para retirar el kit de competencia debe presentar su cédula de identidad. Las personas que se encuentren imposibilitadas para efectuar el retiro pueden enviar a un representante con un poder simple.
</p>
                            <p>Los corredores menores de 18 años de edad deberán hacer retiro de su Kit de competencia acompañados de su padre o algún representante legal, que firme la autorización a participar y la liberación de responsabilidades correspondiente. </p>
                            
                            <p>El uso de la polera oficial entregada en kit de competencia es obligatorio para todos los corredores, esto es requisito fundamental para optar a premiación.</p>
                            
                            <p>Los números deben colocarse obligatoriamente en el pecho, completamente extendidos, sin manipular, modificar o doblar hasta el término de la carrera.</p>
                            </article>

<div class="anchor" id="articulo6"></div>
<article class="articulo">
                                <h3>ARTÍCULO 6° Premios.</h3>
                            
                            <p>En todas las categorías se premiara a los primeros tres lugares con medallas y obsequios otorgados por los auspiciadores. Estos últimos son de absoluta responsabilidad de la empresa auspiciadora, por ende, todo participante exime de cualquier tipo de responsabilidad a la organización. </p>
                            </article>

<div class="anchor" id="articulo7"></div>
<article class="articulo">
                                <h3>ARTÍCULO 7° Obligaciones del participante.</h3>
                                <p>Serán descalificados los/las participantes, en los siguientes casos:
                                
                                    <ul>
                                    	<li>Que no realicen el recorrido oficial trazado por la organización de la prueba.</li>
                                    	<li>Quienes no lleven su número en forma visible.</li>
                                    	<li>Los que manifiesten un comportamiento antideportivo.</li>
                                    	<li>Acciones que vulneren la seguridad física de otros competidores.</li>
                                    	<li>Suplantación de personal y/o correr una distancia distinta a la indicada en la inscripción.</li>
                                    	<li>No cruzar por los puntos de control de la ruta, cruzar por veredas, o zonas ajenas al trazado.</li>
                                    </ul>
                                </p>
                            
                                <p>Todo atleta deberá realizarse un chequeo médico con anterioridad a la competencia donde se declare que es apto para realizarla.</p>   
                            
                                <p>Todo aquel individuo, sea participante o no, que corra sin número e ingrese a la ruta sin autorización de la organización, lo hace bajo su responsabilidad, sin tener derecho a ninguna de las prerrogativas a las que tienen derecho los /las atletas oficialmente inscritos. Sin perjuicio del derecho que le asiste a la organización de impedir su participación en el evento.</p>
                            </article> 

<div class="anchor" id="articulo8"></div>
<article class="articulo">
                                <h3>ARTÍCULO 8° Servicios al participante.</h3>
                            
                            <ol>
                            	<li>Existirán puestos de abastecimiento (hidratación) en el punto de partida y a lo largo del recorrido. </li>
                            	<li>Existirá un servicio de aprovisionamiento en la llegada y guardarropía (no responsabilizándose la organización de los objetos entregados en él).</li>
                            	<li>El día del evento habrán estacionamientos, limitados, disponibles al interior de la Scuola Italiana.</li>
                            	<li> Existirán baños disponibles para los atletas en las dependencias de la Scuola Italiana.</li>
                            	<li>En cuanto a las atenciones ante posibles urgencias médicas, se contará con los medios tanto humanos como materiales para hacerles frente. Los gastos incurridos por eventos de este tipo son de cargo del participante.</li>
                            </ol>
                            </article>

<div class="anchor" id="articulo9"></div>
<article class="articulo">
                                <h3>ARTÍCULO 9° Reclamos.</h3>
                            
                            <p>Los reclamos podrán ser realizados antes de cumplirse 30 minutos desde la publicación de los resultados, en forma verbal por el participante al director de prueba o a quién la organización designe para recibir reclamos, quien deberá resolver en el acto el reclamo interpuesto. De no estar de acuerdo el participante con lo resuelto por el director de prueba o por quien la organización designe, podrá reclamar al Jurado de Apelación, inmediatamente, por nota firmada.</p>
                            
                            <p>El jurado estará conformado por el director de la competencia y el rector de la Scuola Italiana, y actuará como árbitro arbitrador, sin forma de juicio, resolviendo los reclamos que los participantes hagan de acuerdo al artículo 10º en el menor plazo posible y luego de oír a los interesados.</p>
                            </article>

<div class="anchor" id="articulo10"></div>
<article class="articulo">
                                <h3>ARTÍCULO 10° Vehículos en el recorrido.</h3>
                            <p>Los únicos vehículos autorizados a seguir la prueba son los designados por la organización, y los de Carabineros. Queda totalmente prohibido seguir a los participantes en moto, bicicleta u otro vehículo teniendo orden expresa Carabineros de Chile de retirarlos del circuito, todo lo anterior para evitar accidentes. </p>
                            </article>

<div class="anchor" id="articulo11"></div>
<article class="articulo">
                                <h3>ARTÍCULO 11° Responsabilidades</h3>
                            
                            <p>La organización no asume ninguna responsabilidad por los daños que pueda ocasionarse un corredor a sí mismo, a otros participantes o que terceras personas pudiesen ocasionarle durante la competencia. Los inscritos oficialmente como participantes declaran que aceptan la condición anteriormente expuesta y contribuirán con la organización a evitar accidentes, en beneficio de la integridad de todas las personas y sus familias. Todos los participantes declaran conocer y aceptar el presente Reglamento y Pliego de Descargo de Responsabilidades y Protección de Datos. En caso de duda prevalecerá el criterio de la organización.</p>
                            </article>

<div class="anchor" id="articulo12"></div>
<article class="articulo">
                                <h3>Pliego de Descargo de Responsabilidades y Protección de datos.</h3>
                            
                            <p>Por el sólo hecho de inscribirse, el participante declara lo siguiente:</p>
                            
                            <p><em>“Me encuentro en estado de salud óptima de participar en la IV Corrida Italiana. Además eximo de toda responsabilidad a la organización, auspiciadores, patrocinadores u otras instituciones participantes, ante cualquier accidente o lesión que pudiera sufrir antes, durante y/o después del evento deportivo, renunciando desde ya a cualquier acción legal en contra de dichas entidades públicas o privadas.</em></p>
                            
                            <p><em>Entiendo y me encuentro conforme con las condiciones establecidas precedentemente, junto con las responsabilidades que como participante tengo y declaro que no haré responsable a la organización por incumplimientos propios.</em></p>
                            
                            <p><em>Durante el desarrollo de la competencia, contribuiré en lo posible con la organización para evitar accidentes personales. Autorizo además, a que la organización haga uso publicitario de fotos, videos y cualquier otro tipo de material audiovisual en que pueda figurar, aceptando la publicación de mi nombre en la clasificación de la prueba, en los medios de comunicación social y/o Internet, sin esperar pago, compensación o retribución alguna por este concepto. Acepto que lo anterior es condición necesaria para retirar el número de competencia y participar en la Corrida Italiana año 2014.”</em></p>
                            </article>
   