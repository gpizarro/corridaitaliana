<h1>Contacto</h1>
    
    <?php if(!empty($errors) ) { render('form-errors', ['errors' => $errors ]); }?>
                	            
    <form class="navbar-form row" role="form" method="post" action="?location=contactar">

        <div class="form-group">
        
            
            <div class="well clearfix">

                <h2>Cuentanos...</h2>

                <div class="form-group">
                	
                    <label for="nombre">Nombre:</label>
                	<input type="text" class="form-control" placeholder="" id="nombre" name="nombre" value="<?= old('nombre') ;?>">
                		                                        	
                </div>
                                                           

            	<div class="form-group">

            	    <label for="email">Correo Electrónico:</label>
            	    <input type="email" class="form-control" placeholder="" id="email" name="email" value="<?= old('email') ;?>">

            	</div>
                                                        
                	
            	<div class="form-group">
            	
               	     <label for="telefono">Telefono:</label>
                     <input type="text" class="form-control" placeholder="" id="telefono" name="telefono" value="<?= old('telefono') ;?>">                                 	    
            	   
            	</div>
                    
                                                     
                <div class="form-group">
                    
                    <label for="mensaje">Tu mensaje:</label><br>
                    <textarea name="mensaje" id="mensaje" cols="50" rows="10"><?= old('mensaje') ;?></textarea>
                    
                </div>
                
                <div class="form-group">
                    
                    <input type="submit" class="btn btn-lg" value="Enviar mensaje">
                    
                </div>
            
            </div>

            
            
        </div>

        

    </form>