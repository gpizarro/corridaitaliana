<form action="" class="form" method="post">
    <div class="row">
        
        <?php if($unpaid_count == 0) : ?>
            
        <div class="col-md-6">
    		<div class="alert alert-success" role="alert">
    		    <p>¡Todos los inscritos han pagado!</p>
    		</div>
        </div>

            
        <?php else : ?>
        
        <div class="col-md-4">
          	<div class="panel panel-info">
          		<div class="panel-body text-center">
          	      <p class="big-ass-number"><?= $unpaid_count ;?></p>
          			<h3><br>No han pagado</h3>
          			<a href="?location=admin&action=enviar-recordatorio" class="btn btn-lg btn-primary">Enviarles recordatorio</a>
          		</div>
            </div>
        </div>
        
        <?php endif ;?>
    
        
        <?php if($unpaid_count > 0) :?>
            <div class="col-md-4">
      	<div class="panel panel-info">
      		<div class="panel-body text-center">
      		  <p class="big-ass-number" id="counter">

          		  
      		  </p>

      		  <h3>Corredores seleccionados</h3>
      		  
      		  	<button type="submit" class="btn btn-lg btn-primary" id="#actualizarPagos">Actualizar Pagos</button></div>
      		  
      	</div>
    </div>
        <?php endif ;?>
    
    </div>
    
    <br>
    <div class="table-responsive">
    <table class="table table-striped tabla-inscritos">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Apellido 2</th>
          <th>RUT</th>
          <th class="text-center">Pagado</th>
          <th>Acción</th>
        </tr>
      </thead>
      <tbody>
      
      <?php foreach($corredores as $corredor) :?>
            <tr>
                <td><?= $corredor->nombre ;?></td>
                <td><?= $corredor->apellido_paterno ;?></td>
                <td><?= $corredor->apellido_materno ;?></td>
                <td><?= $corredor->rut ;?></td>
                
                <?php if($corredor->pagado == NULL) : ?>
                    <td>
                    	<input type="checkbox" name="pagado[]" class="checkbox" value="<?= $corredor->id ;?>">
                    </td>
                <?php else :?>
                    <td class="text-center">Sí</td>
                <?php endif ;?>
                
                
                <td><a href="?location=admin&action=ver-perfil&id=<?= $corredor->id ;?>">Ver Perfil</a></td>
                
            </tr>
      <?php endforeach ;?>
    
    </tbody>
    </table>
  </div>
</form>
