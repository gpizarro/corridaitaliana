<?php if (isset($alert))  :?>
<div class="row">
	<div class="col-md-6">
		<div class="alert alert-success" role="alert">
		    <p>El pago ha sido actualizado.</p>
		</div>
	</div>
</div>
<?php endif ;?>

<div class="container">

    <div class="row">
    
        
        <div class="panel panel-warning col-md-6">
    		
    		<div class="panel-body">
    		    
    		    <h2><?= $corredor->nombre.' '.$corredor->apellido_paterno ;?> &nbsp;<small>(<?= ucfirst(  $corredor->tipo_corredor  );?>)</small></h2>
    	                
    	        <h3></h3>
    	        
                <h3>Distancia: &nbsp; <small><?= $corredor->distancia ;?></small></h3>
    	        			
    			<h3>Talla: &nbsp; <small><?= strtoupper( $corredor->talla );?></small></h3>
    			
    			 
    			 <?php if ($corredor->pagado !== NULL) : ?>
    			 
    			    <h3>Numero: &nbsp;  <small>Poner número acá</small></h3>
    			 
    			 <?php else : ?>
                    
                    <br>
                    <form action="#" class="form" method="post">
                        <input type="hidden" name="pagado" value="1">
                    	<button type="submit" class="btn btn-primary btn-lg">Actualizar Pago</button>
                    </form>
    			 	
    			 <?php endif ;?>
    			 			
    		</div>
    		
    	</div>
	
	</div>

</div>

<form class="form-horizontal" role="form">

    
    <div class="well clearfix">
        
        <h2>Datos Personales &nbsp; <small><a href="">Editar</a></small></h2>
    	
        <div class="row">
            	<div class="col-md-6">
            	    	  	
            	<!-- Nombre -->
            	<div class="form-group ">
            	    	  	
            	    	  	  <label class="col-sm-2 control-label">Nombre</label>
            	    	  	  
            	    	  	  <div class="col-sm-10">
            	    	  	      <p class="form-control-static"><?= $corredor->nombre ;?></p>
            	    	  	  </div>
            	    	  	
            	    	  	</div>
            	
            	<!-- Apellido Paterno -->
            	<div class="form-group">
            	    	  		<label for="apellido_paterno" class="col-sm-2 control-label">Apellido Paterno</label>
            	    	  		<div class="col-sm-10">
            	    	  			<p class="form-control-static"><?= $corredor->apellido_paterno ;?></p>
            	    	  		</div>
            	    	  	</div>
            	
            	<!-- Apellido Materno -->
            	<div class="form-group">
            	  	    		<label for="apellido_materno" class="col-sm-2 control-label">Apellido Materno</label>
            	  	    		<div class="col-sm-10">
            	  	    			<p class="form-control-static"><?= $corredor->apellido_materno ;?></p>
            	  	    		</div>
            	  	    	</div>
            	
            	<!-- Sexo -->
            	<div class="form-group">
            		    		<label for="sexo" class="col-sm-2 control-label">Sexo:</label>
            		    		<div class="col-sm-10">
            		    			<p class="form-control-static"><?= $corredor->sexo ;?></p>
            		    		</div>
            		    	</div>
            	
            	    	
            	         </div>
            	         
            	<div class="col-md-6">
            	         
            	<div class="form-group">
            		<label for="fecha_nacimiento" class="col-sm-2 control-label">Fecha Nacimiento:</label>
            		<div class="col-sm-10">
            			<p class="form-control-static"><?= $corredor->fecha_nacimiento ;?></p>
            		</div>
            	</div>
            	 
            	 <!-- Telefono -->
            	 <div class="form-group">
            	 	<label for="telefono" class="col-sm-2 control-label">Telefono</label>
            	 	<div class="col-sm-10">
            	 		<p class="form-control-static"><?= $corredor->telefono ;?></p>
            	 	</div>
            	 </div>
            	 
            	 <!-- Rut -->
            	 <div class="form-group">
            	  	    		<label for="rut" class="col-sm-2 control-label">RUT:</label>
            	  	    		<div class="col-sm-10">
            	  	    			<p class="form-control-static"><?= $corredor->rut ;?></p>
            	  	    		</div>
            	  	    	</div>
            	    	
            	 <!-- Email -->
            	 <div class="form-group">
            	    	  	    <label class="col-sm-2 control-label">Email</label>
            	    	  	    <div class="col-sm-10">
            	    	  	      <p class="form-control-static"><?= $corredor->email ;?></p>
            	    </div>
            	    	    </div>
            	
            	         </div>
            </div>
            
        <h2>Datos Corrida:</h2>

        <div class="row">
            	<div class="col-md-6">
                	<div class="form-group">
                		<label for="pagado" class="col-sm-2 control-label">¿Pagó?:</label>
                		<div class="col-sm-10">
                			<p class="form-control-static">
                    			
                    			<?= $corredor->pagado !== NULL ? 'Sí' : 'No' ;?>
                    			
                			</p>
                		</div>
                	</div>
            	</div>
            </div>
             
    </div>


</form>