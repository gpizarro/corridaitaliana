<?php

define('ROOT', dirname(__FILE__));

require ROOT.'/../vendor/autoload.php';



$env = $_SERVER['HTTP_HOST'];


switch( $env ){
    
    case 'corridaitaliana.dev':
    
        define('HOST','/');
        
        ActiveRecord\Config::initialize(function($cfg)
        {
            $cfg->set_model_directory(ROOT.'/Corrida');
            $cfg->set_connections(['development' => 'mysql://root:root@localhost/corrida_italiana?charset=utf8']);
            
        });
            
    break;
    
    
    case 'stage.gpizarro.me':
    
        define('HOST', 'http://stage.gpizarro.me/corridaitaliana/');
    
    break;
    
    
    case 'corridaitaliana.cl':
    
        define('HOST', 'http://corridaitaliana.cl/');
        
        ActiveRecord\Config::initialize(function($cfg)
        {
            $cfg->set_model_directory(ROOT.'/Corrida');
            $cfg->set_connections([
            
                'tests'      => 'mysql://corridai_web:EtZwLaQXTszAwr6y@localhost/corridai_corrida_2014_tests?charset=utf8',
                'production' => 'mysql://corridai_web:EtZwLaQXTszAwr6y@localhost/corridai_2014?charset=utf8'
                
                ]);
                
             $cfg->set_default_connection('production');
            
        });
        
    
    break;
    
     case 'www.corridaitaliana.cl':
         
        define('HOST', 'http://www.corridaitaliana.cl/');
        
        ActiveRecord\Config::initialize(function($cfg)
        {
            $cfg->set_model_directory(ROOT.'/Corrida');
            $cfg->set_connections([
            
                'tests'      => 'mysql://corridai_web:EtZwLaQXTszAwr6y@localhost/corridai_corrida_2014_tests?charset=utf8',
                'production' => 'mysql://corridai_web:EtZwLaQXTszAwr6y@localhost/corridai_2014?charset=utf8'
                
                ]);
                
             $cfg->set_default_connection('production');
            
        });
        
    
    break;
    
    
}

//Load Helpers
require ROOT.'/functions.php';
