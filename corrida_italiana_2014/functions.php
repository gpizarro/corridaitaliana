<?php 
/**

**********************
 
functions.php file


- Helper functions 
**********************

*/


/** 

Basic Loading

*/

function render($template, $data = [] ) {
   
   $path = ROOT.'/views/partials/'.$template.'.php';
   
   if (file_exists($path))
    {
        extract($data);
        require($path);
    }
};

function load($template, $data = [] ) {
   
   $path = ROOT.'/views/content/'.$template.'.php';
   
   if (file_exists($path))
    {
        extract($data);
        require($path);
    }
};

function load_module($template, $data = [] ) {
   
   $path = ROOT.'/views/content/modules/'.$template.'.php';
   
   if (file_exists($path))
    {
        extract($data);
        require($path);
    }
};


function render_admin($template, $data = [] ) {
   
   $path = ROOT.'/views/admin/partials/'.$template.'.php';
   
   if (file_exists($path))
    {
        extract($data);
        require($path);
    }
};


function load_admin($template, $data = [] ) {
   
   $path = ROOT.'/views/admin/content/'.$template.'.php';
   
   if (file_exists($path))
    {
        extract($data);
        require($path);
    }
};


/**

Template Loading 
*****************

*/

function load_base_template_with($content, $data = []) {

    
    render('head', $data);
    render('navbar');
    
    render('template_open', $data);

    load($content, $data);
    
    render('template_close');
    render('footer');
    
    
}

function load_admin_base_template($content, $data = []) {
    
    render('head', $data);

    render('navbar');
    
    render_admin('sidebar');
    
    render_admin('template_open', $data);
        
    load_admin($content, $data);
    
    render_admin('template_close');
    
    render_admin('footer');
    
}


/**

Sponsor Logo Load 
******************

*/

function show_sponsor($sponsor){
    echo 
    "<a href='#' class='sponsor-link'>"
    ."<img src=".HOST."images/sponsors/".$sponsor." alt='' class='img-responsive sponsor-img' />"
    ."</a>";

} 

/********
*
* HTML 
*
*********/

function link_to($path, $path_name, $class = '') {
    
    $class =  ' class='.$class;
    
    echo
    "<a href='?location=$path' $class>$path_name</a>";
    //"<a href='$path' $class>$path_name</a>";
}


function mail_to($path, $show) {
    
    echo
    "<a href='mailto:$path'>$show</a>";
}


/****************
*
*  Validation
*
*****************/

function old($key) {
    
    if ( !empty($_REQUEST[$key] ) ) {
        
        return htmlspecialchars($_REQUEST[$key]);
        
    }
    
    return '';
    
}


function old_radio($name, $value) {
                    	     
    if(isset($_POST[$name]) && $_POST[$name] == $value)  {
     
     echo 'checked';
     
    }

}



function load_controller($controller) {
    
    $path = ROOT.'/controllers/'.$controller.'-controller.php';
    
    if (file_exists($path)) {
        
        require($path);
    }
    
}


