<?php 

/**
*
*  CONTACT VALIDATOR
*
*  Valida el contenido de la página de contacto 
* 
*/


// Load the data
require ROOT.'/Corrida/contact/contact_data.php';


if ($_SERVER['REQUEST_METHOD'] == "POST" ) {
        
        
    $errors = [];
    
    if( empty($nombre) ){
        
        $error = 'Necesitamos saber tu nombre';
                
        array_push($errors, $error);
        
    }
    
    
    if( empty($email) ){
        
        $error = 'Necesitamos saber tu correo electrónico';
        
         array_push($errors, $error);
        
    }
    
    
    if( empty($telefono) ){
        
        $error = 'Necesitamos saber tu teléfono';
        
         array_push($errors, $error);
        
    }
    
    if( empty($mensaje) ){
        
        $error = 'Tu mensaje está vacío';
        
         array_push($errors, $error);
        
    }
                
        
}
    
        
if ( empty($errors) ) {
    
    $validated = true;
    
    return $validated;
     
} else {
    
    $validated = false;
    
    return $validated;
    
}

