<?php 

/**
* Registration Data
* 
* Assigns form inputs to variables
*/



$nombre           = $_POST['nombre'];
$apellido_paterno = $_POST['apellido_paterno'];
$apellido_materno = $_POST['apellido_materno'];
$fecha_nacimiento = $_POST['fecha_nacimiento'];
$rut              = $_POST['rut'];
$telefono         = $_POST['telefono'];
$email            = $_POST['email'];


// Sexo
if (isset($_POST['sexo'] )) {
    
    $sexo = $_POST['sexo'];
    
}

// Carrera
if (isset($_POST['distancia'] )) {
    
    $distancia = $_POST['distancia'];
    
}

// Talla Polera
if (isset($_POST['talla'] )) {
    
    $talla = $_POST['talla'];
    
}


// Tipo Corredor
if (isset($_POST['tipo_corredor'] )) {
    
    $tipo_corredor = $_POST['tipo_corredor'];
    
}
