<?php 


/**
*
*  REGISTRATION VALIDATOR
*
*  Valida el contenido de la página de registro 
* 
*/


// Load the data
require(ROOT.'/Corrida/registration/registration_data.php');


if ($_SERVER['REQUEST_METHOD'] == "POST" ) {
        
        
        $errors = [];
        
        if( empty($nombre) ){
            
            $error = 'Necesitamos saber tu nombre';
                    
            array_push($errors, $error);
            
        }
        
        
         if( empty($apellido_paterno) ){
            
            $error = 'Necesitamos saber tu apellido paterno';
            
             array_push($errors, $error);
            
        }
        
        if( empty($apellido_materno) ) {
            
            $error = 'Necesitamos saber tu apellido materno';
            
             array_push($errors, $error);
            
        }
        
        if ( !isset($sexo) ) {
            
            $error = 'Debes decirnos si eres hombre o mujer';
    
            array_push($errors, $error);
        }
        
        
        if( empty($fecha_nacimiento) ) {
            
            $error = 'Necesitamos saber tu fecha de nacimiento';
            
             array_push($errors, $error);
            
        }
        
        
        if( empty($rut) ) {
            
            $error = 'Necesitamos saber tu RUT';
            
             array_push($errors, $error);
            
        }
        
        
        if( empty($telefono) ){
            
            $error = 'Necesitamos saber tu teléfono';
            
             array_push($errors, $error);
            
        }
        
        if( empty($email) ){
            
            $error = 'Necesitamos saber tu correo electrónico';
            
             array_push($errors, $error);
            
        }
        
        
        if( empty($distancia) ){
            
            $error = 'Necesitamos saber qué distancia vas a correr';
            
             array_push($errors, $error);
            
        }
        
        
        if( empty($talla) ){
            
            $error = 'Necesitamos saber tu talla';
            
             array_push($errors, $error);
            
        }
        
        if ( !isset($tipo_corredor) ) {
            
            $error = 'Debes decirnos qué tipo de corredor eres';
    
            array_push($errors, $error);
        }
        
        
    }
    
        if ( empty($errors) ) {
            
            $validated = true;
            return $validated;
             
        } else {
            
            $validated = false;
            return $validated;
            
        }

