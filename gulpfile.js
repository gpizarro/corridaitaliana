// Gulpfile.js
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var imageop = require('gulp-image-optimization');

var imagePath = 'public_html/images/*';

gulp.task('css', function(){
    gulp.src('sass/corrida_italiana.scss')
        .pipe(sass())
        .pipe(autoprefixer('last 5 versions'))
        .pipe(gulp.dest('public_html/css'));
}); 


gulp.task('images', function(cb) {
    gulp.src(['public_html/images/**/*.png','public_html/images/**/*.jpg','public_html/images/**/*.gif','public_html/images/**/*.jpeg'])
        .pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })).pipe(gulp.dest('public_html/images')).on('end', cb).on('error', cb);
});

gulp.task('watch', function() {
    gulp.watch('sass/corrida_italiana.scss', ['css'])
});

gulp.task('default', ['watch', 'images']);