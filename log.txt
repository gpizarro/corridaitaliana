******************
* Log de avances
******************

- Revisar pq no se manda el correo de reminder desde production site


10 de octubre:

- Se corrigió la pagina admin->general.  El numero de total inscritos, no han pagado y pagaron reciben info de la base de datos. 

- Corregir el monto en confirmaciones de registro (write that logic)
    ->  Duda: Nunca se especifica en la página el monto que debe pagar.  Esto podria causar confusión entre varias 
        personas que se hayan registrado y vean que tienen que pagar diferente.  
        
            -> Se puede solucionar poniendo el monto junto al tipo de corredor al momento de inscribirse.
            
- Declarar haber leido declaracion jurada al momento de inscribirse
    -> No hay acción porque está ubicada abajo al lado del boton de inscripción.
    
- Cambio del CTA en inscripción de "Regisrtate" a "Inscribete"

- Habilitado el correo de recordatorio de pago masivo

- Se puede ver el perfil de cada persona y actualizar si pago directamente desde ahi. 

Preguntas: 
**********
¿Se va a necesitar ingresar el monto en la base de datos?  ¿Habrá algun tipo de sistema de recaudo o algo parecido? 

Pendientes:
************

Prioridad A:

    - Email de confirmacion de PAGO
        -> Debe incluir el numero del corredor. 
    
    - Seccion de contactos en admin
    
    - Revertir el status de pago en caso que haya habido un error. 


Prioridad B:

    - Facilitar el interface en el registro:
        - Botones Mobile Friendly para los radio inputs y checklists.
        
    - Definir el contenido de  'Auspiciadores'
    
    
    - Dar la opcion de descargar la ruta
    
    - arreglar formato de fechas para latinos

    - redirigir a inscripcion si intentan ingresar directamente desde el URL (?location=registrar)

Prioridad C:

    - que se vea bonito el correo en gmail.
    
    - Arreglar el counter en parte de inscritos.  El counter no regresa a 0 si se deseleccionan todos. 
    
    - Countdown al dia de la corrida